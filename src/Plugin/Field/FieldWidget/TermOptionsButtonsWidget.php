<?php

namespace Drupal\taxonomy_depth_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'term_depth_options_buttons' widget.
 *
 * @FieldWidget(
 *   id = "term_depth_options_buttons",
 *   label = @Translation("Term Depth Check boxes/radio buttons"),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class TermOptionsButtonsWidget extends OptionsWidgetBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a TermOptionsSelectWidget widget.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'deepest' => FALSE,
      'depth_range' => FALSE,
      'min_depth' => '',
      'depth' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['deepest'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Deepest elements.'),
      '#description' => $this->t('Get the last level of this vocabulary (if there are elements with different depths is going to get the deepest).'),
      '#default_value' => $this->getSetting('deepest'),
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][depth_range]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $element['depth_range'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Set range between depths.'),
      '#description' => $this->t('Check your vocabulary/vocabularies tree to set properly the range.'),
      '#default_value' => $this->getSetting('depth_range'),
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][deepest]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $element['min_depth'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum depth of the taxonomy tree'),
      '#description' => $this->t('Should be less value than depth max field.'),
      '#default_value' => $this->getSetting('min_depth'),
      '#min' => 1,
      '#max' => 15,
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][depth_range]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][depth_range]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['depth'] = [
      '#type' => 'number',
      '#title' => $this->t('Depth/max depth of the taxonomy tree'),
      '#description' => $this->t('Set 0 for all levels if you are not using the depth ranges'),
      '#default_value' => $this->getSetting('depth'),
      '#required' => TRUE,
      '#min' => 0,
      '#max' => 15,
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][deepest]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $element['#element_validate'][] = [
      $this,
      'taxonomyDepthSelectRangeWidgetValidate',
    ];
    return $element;
  }

  /**
   * Custom validation to depth ranges.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Form state.
   */
  public function taxonomyDepthSelectRangeWidgetValidate(array $element, FormStateInterface $form_state) {
    if ($element['depth_range']['#value']) {
      $min_depth = $element['min_depth']['#value'];
      $max_depth = $element['depth']['#value'];

      if ($min_depth > $max_depth) {
        $form_state->setErrorByName('min_depth', $this->t('The minimum depth can not be higher than the maximum depth.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    if ($this->getSetting('deepest')) {
      $summary[] = $this->t('Deepest elements');
    }
    elseif ($this->getSetting('depth_range')) {
      $summary[] = $this->t('Depths set between @min_depth and @depth', [
        '@min_depth' => $this->getSetting('min_depth'),
        '@depth' => $this->getSetting('depth'),
      ]);
    }
    else {
      $summary[] = $this->t('Taxonomy depth: @depth', ['@depth' => $this->getSetting('depth')]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $field_settings = $items->getFieldDefinition()->getSettings();
    if ($field_settings["target_type"] == 'taxonomy_term') {
      $options = [];
      $min_depth = NULL;
      $deepest = $this->getSetting('deepest') ?: FALSE;
      $max_depth = ($this->getSetting('depth') != 0) ? $this->getSetting('depth') : NULL;
      $max_depth = $deepest ? NULL : $max_depth;
      if (!$deepest && $this->getSetting('depth_range')) {
        $min_depth = ($this->getSetting('min_depth') == 0) ? 0 : $this->getSetting('min_depth') - 1;
      }

      $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple($field_settings["handler_settings"]["target_bundles"]);
      /** @var \Drupal\taxonomy\TermStorageInterface $taxonomy_storage */
      $taxonomy_storage = $this->entityTypeManager->getStorage('taxonomy_term');

      // Search the deepest value.
      if ($deepest) {
        $max_depth_value = 0;
        foreach ($vocabularies as $vocabulary) {
          $terms = $taxonomy_storage->loadTree($vocabulary->id(), 0, $max_depth);
          foreach ($terms as $term) {
            if ($term->depth > $max_depth_value) {
              $max_depth_value = $term->depth;
            }
          }
        }
        $max_depth = $max_depth_value + 1;
      }

      foreach ($vocabularies as $vocabulary) {
        $terms = $taxonomy_storage->loadTree($vocabulary->id(), 0, $max_depth);

        foreach ($terms as $term) {
          if ($deepest && $term->depth == ($max_depth - 1)) {
            $options[$term->tid] = str_repeat('-', $max_depth) . $term->name;
          }
          elseif (!$deepest && (($min_depth || $min_depth == 0) && $min_depth <= $term->depth || is_null($min_depth))) {
            $term_depth = (is_null($min_depth) || $term->depth == 0) ? $term->depth : $term->depth - $min_depth;
            $options[$term->tid] = str_repeat('-', $term_depth) . $term->name;
          }
        }
      }
    }
    else {
      $options = $this->getOptions($items->getEntity());
    }

    $selected = $this->getSelectedOptions($items);

    // If required and there is one single option, preselect it.
    if ($this->required && count($options) == 1) {
      reset($options);
      $selected = [key($options)];
    }

    if ($this->multiple) {
      $element += [
        '#type' => 'checkboxes',
        '#default_value' => $selected,
        '#options' => $options,
      ];
    }
    else {
      $element += [
        '#type' => 'radios',
        // Radio buttons need a scalar value. Take the first default value, or
        // default to NULL so that the form element is properly recognized as
        // not having a default value.
        '#default_value' => $selected ? reset($selected) : NULL,
        '#options' => $options,
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    if (!$this->required && !$this->multiple) {
      return $this->t('N/A');
    }
  }

}
