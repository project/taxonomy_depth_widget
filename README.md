Taxonomy Depth Widget

## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

This module allows you to select a depth or a depths range
from your taxonomy tree items for your field in the
"Manage form display" section.


 * For a full description:
   https://drupal.org/project/taxonomy_depth_widget

 * Issue queue for Group Webform:
   https://drupal.org/project/issues/taxonomy_depth_widget

## REQUIREMENTS

 * Taxonomy (Drupal core).


## INSTALLATION

  * Install normally as other modules are installed. For support:
    https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules


## CONFIGURATION

 * In your entity go to "Manage form display",
 find your taxonomy entity reference field,
 change the widget to "Term Depth Select list" or
 "Term Depth Check boxes/radio buttons" and set the depth.


## MAINTAINERS

Current maintainers:
 * Fabian Sierra (fabiansierra5191) - https://www.drupal.org/u/fabiansierra5191
